;
// global.js
const HIDE_CLASS = "visually-hidden";

class ElementClassToggler {
    constructor(firstClass, secondClass, catchClass, destClass) {
        this.firstClass = firstClass;
        this.secondClass = secondClass;
        this.catchClass = catchClass;
        this.destClass = destClass;
    }

    toggleClass(target) {
        let catchElement =  target.closest("." + this.catchClass);
        
        if (catchElement === null) {
            return;
        }

        let destElement = catchElement.closest("." + this.destClass);

        if (destElement.classList.contains(this.firstClass)) {
            this.firstToSecond(destElement);
        } else {
            this.secondToFirst(destElement);
        }
    }

    setEventListener() {
        document.addEventListener("click", e => {
            this.toggleClass(e.target);
        });
    }

    firstToSecond(destElement) {
        destElement.classList.add(this.secondClass);
        destElement.classList.remove(this.firstClass);
    }

    secondToFirst(destElement) {
        destElement.classList.add(this.firstClass);
        destElement.classList.remove(this.secondClass);
    }
}

function throttleFunction(func, msTime) {
    let lastTime = Date.now();
    
    return function(menu, control) {
        let currentTime = Date.now();
        let result = null;
        if ((currentTime - lastTime) >= msTime) {
            result = func(menu, control);
            lastTime = currentTime;
        }
        return result;
    };
}

function showElement(element) {
    element.classList.remove(HIDE_CLASS);
}

function hideElement(element) {
    element.classList.add(HIDE_CLASS);
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, props) {
    props = props || {};
    let exp = props.expires;
    if (typeof exp === "number" && exp) {
        let d = new Date();
        d.setTime(d.getTime() + exp * 1000);
        exp = props.expires = d;
    }

    if (exp && exp.toUTCString) {
        props.expires = exp.toUTCString();
    }

    value = encodeURIComponent(value);
    let updatedCookie = name + "=" + value;

    for (let propName in props) {
        updatedCookie += "; " + propName;
        let propValue = props[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
    document.cookie = updatedCookie;
}

function deleteCookie(name) {
    setCookie(name, null, { expires: -1 });
}

// main-menu.js
(function() {
    const MIN_DESKTOP_WIDTH = 1171;
    const SCROLL_BAR_WIDTH = window.innerWidth - document.documentElement.clientWidth;

    const mainMenuControl = document.getElementById("main-menu__control");
    const mainMenu = document.getElementById("main-menu");
    const mainMenuWrapper = document.getElementById("mainMenuWrapper");
    const mainMenuContent = document.getElementById("mainMenuContent");
    const mainMenuMobile = document.getElementById("mainMenuMobile");
    // const mainMenuControlClosedIcon = document.getElementById("control__icon_closed");
    // const mainMenuControlOpenedIcon = document.getElementById("control__icon_opened");

    setMenuByClientWidth();
    
    let setMenuByClientWidthThrottled = throttleFunction(setMenuByClientWidth, 100);

    window.addEventListener('resize', function() {
        setMenuByClientWidthThrottled();
    });
    
    mainMenuControl.addEventListener("click", function() {
        if (mainMenu.classList.contains("main-menu_opened")) {
            closeMenu();
            setTimeout(() => {
                document.removeEventListener("click", closeMenuFocusout);
            }, 0);
        } else {
            openMenu();
            setTimeout(() => {
                document.addEventListener("click", closeMenuFocusout)
            }, 0);
        }
    });

    function setMenuByClientWidth() {
        if (document.documentElement.clientWidth < MIN_DESKTOP_WIDTH - SCROLL_BAR_WIDTH) {
            closeMenu();
            mainMenuMobile.append(mainMenuContent);
            showElement(mainMenuControl);
        } else {
            mainMenuWrapper.append(mainMenuContent);
            hideElement(mainMenuControl);
        }
    }

    function openMenu() {
        mainMenu.classList.remove("main-menu_closed");
        mainMenu.classList.add("main-menu_opened");
        showElement(mainMenuMobile);
        // hideElement(mainMenuControlClosedIcon);
        // showElement(mainMenuControlOpenedIcon);
    }
    
    function closeMenu() {
        mainMenu.classList.add("main-menu_closed");
        mainMenu.classList.remove("main-menu_opened");
        hideElement(mainMenuMobile);
        // hideElement(mainMenuControlOpenedIcon);
        // showElement(mainMenuControlClosedIcon);
    }

    function closeMenuFocusout(e) {
        if (!e.target.closest(".main-menu__content")) {
            closeMenu();
            document.removeEventListener("click", closeMenuFocusout);
        }
    }
    
})();

// search
(function() {
    const MIN_DESKTOP_WIDTH = 1171;
    const SCROLL_BAR_WIDTH = window.innerWidth - document.documentElement.clientWidth;
    
    const mainSearch = document.getElementById("search");
    const mainSearchSubmit = document.getElementById("search__button_submit");
    const mainSearchReset = document.getElementById("search__button_reset");
    const mainSearchInput = document.getElementById("search__input");
    let isBlur = false;

    setSearchByClientWidth(mainSearchInput);

    let setSearchByClientWidthThrottled = throttleFunction(setSearchByClientWidth ,100);

    window.addEventListener("resize", function() {
        setSearchByClientWidthThrottled(mainSearchInput)
    });

    mainSearchSubmit.addEventListener("mousedown", function() {
        if (isBlur) {
            isBlur = false;
        }
    });

    mainSearchSubmit.addEventListener("click", function(e) {
        if (document.documentElement.clientWidth < MIN_DESKTOP_WIDTH - SCROLL_BAR_WIDTH) {

            if (!mainSearch.classList.contains("search_opened") && !isBlur) {
                e.preventDefault();
                mainSearch.classList.add("search_opened");
                mainSearchInput.classList.remove(HIDE_CLASS);
                mainSearchInput.focus();
            }
        }
    });

    mainSearch.addEventListener("reset", function() {
        mainSearchInput.focus();
        mainSearchReset.classList.add(HIDE_CLASS);
    })

    mainSearchInput.addEventListener("input",  function() {
        if (mainSearchInput.value === "") {
            mainSearchReset.classList.add(HIDE_CLASS);
        } else {
            mainSearchReset.classList.remove(HIDE_CLASS);
        }
    });

    mainSearchInput.addEventListener("blur", function(e) {
        if (document.documentElement.clientWidth < MIN_DESKTOP_WIDTH - SCROLL_BAR_WIDTH) {
            if (mainSearchInput.value === "" && (e.relatedTarget === null || e.relatedTarget.classList.contains("search__button_submit"))) {
                mainSearch.classList.remove("search_opened");
                mainSearchInput.classList.add(HIDE_CLASS);
                isBlur = true;
            }
        }
    });

    function setSearchByClientWidth(input) {
        if (document.documentElement.clientWidth < MIN_DESKTOP_WIDTH - SCROLL_BAR_WIDTH) {
            input.classList.add(HIDE_CLASS);
        } else {
            input.classList.remove(HIDE_CLASS);
        }
    }
})();

// card-list
(function() {
    const cardList = document.getElementById("card-list__list");
    const cardListWrapper = document.getElementById("card-list__wrapper");
    const MOVE_ERROR = 10; // погрешность движения
   
    let maxLeftOffset = cardList.clientWidth - cardList.parentElement.clientWidth;
    let startTranslateX = 0;
    let deltaX = 0;
    
    let {downEvent, upEvent, moveEvent} = setCardListOnDisplayType();



    cardListWrapper.addEventListener("click", function(e) {
        if (e.target.closest(".card-list__link") !== null && (deltaX > MOVE_ERROR || deltaX < -MOVE_ERROR)) { // 10 это погрешность
            e.preventDefault();
        }
        deltaX = 0;
    });

    cardListWrapper.addEventListener(downEvent, downPointHandler)

    let setCardListOnResizeThrottled = throttleFunction(setCardListOnResize, 100);
    window.addEventListener("resize", setCardListOnResizeThrottled);

    function downPointHandler(e) {
        let startX = (typeof e.touches !== "undefined") ? e.touches[0].pageX : e.pageX;
        cardList.style.transition = "unset";

        document.addEventListener(moveEvent, moveEventHandler);
        document.addEventListener(upEvent, upEventHandler);

        function moveEventHandler(e) {
            onMoveEvent(e, startX);
        }
        
        function onMoveEvent(e, startX) {
            let evtPageX = (typeof e.touches !== "undefined") ? e.touches[0].pageX : e.pageX;
            deltaX = coordinateDelta(startX, evtPageX);
            cardList.style.transform = "translateX(" + (deltaX + startTranslateX) + "px)";
        }

        function upEventHandler(e) {
            startTranslateX += deltaX;
            if (maxLeftOffset < 0 || startTranslateX > 0) {
                cardList.style.transition = "transform 0.5s ease 0s";
                cardList.style.transform = "translateX(" + 0 + "px)";
                startTranslateX = 0;
            } else if (startTranslateX < -maxLeftOffset) {
                cardList.style.transition = "transform 0.5s ease 0s";
                cardList.style.transform = "translateX(" + -maxLeftOffset + "px)";
                startTranslateX = -maxLeftOffset;
            }
          
            document.removeEventListener(moveEvent, moveEventHandler);
            document.removeEventListener(upEvent, upEventHandler);
        }

    }

    function coordinateDelta(start, current) {
        return current - start;
    }

    function setCardListOnDisplayType() {
        let mainControlEvents = {};

        if ('ontouchstart' in window || navigator.msMaxTouchPoints) {
            mainControlEvents.downEvent = "touchstart";
            mainControlEvents.upEvent = "touchend";
            mainControlEvents.moveEvent = "touchmove";
        } else {
            mainControlEvents.downEvent = "mousedown";
            mainControlEvents.upEvent = "mouseup";
            mainControlEvents.moveEvent = "mousemove";
        }

        return mainControlEvents;
    }

    function setCardListOnResize() {
        maxLeftOffset = cardList.clientWidth - cardList.parentElement.clientWidth;
    }
})();

// sort
(function() {
    const sortBlocksMap = new Map();

    class Sort {
        constructor() {
            this.currentActive = null;
        }
        setCurrentActive(sortButton) {
            if (this.currentActive === null) {
                this.currentActive = sortButton;
                sortButton.classList.add("sort__button_active");
                this.showSortIcon(this.currentActive.querySelector(".sort__icon"));
                return;
            }

            if (this.currentActive !== sortButton) {
                this.currentActive.classList.remove("sort__button_active");
                this.hideSortIcon(this.currentActive.querySelector(".sort__icon"));
                this.currentActive = sortButton;
                this.showSortIcon(this.currentActive.querySelector(".sort__icon"));
                this.currentActive.classList.add("sort__button_active");
            }
            return;
        }
        toggleSortMode(sortButton) {
            if (sortButton !== null) {
                if (sortButton.classList.contains("sort__button_desc")) {
                    sortButton.classList.remove("sort__button_desc");
                    sortButton.classList.add("sort__button_asc");
                } else {
                    sortButton.classList.remove("sort__button_asc");
                    sortButton.classList.add("sort__button_desc");
                }
            }
        }
        showSortIcon(button) {
            button.classList.remove("visually-hidden");
        }
        hideSortIcon(button) {
            button.classList.add("visually-hidden");
        }
    }

    document.addEventListener("click", handleClickSort);

    function handleClickSort(e) {
        let sortButton = e.target.closest(".sort__button");
        if (sortButton !== null) {
            let sort;
            let sortBlock = e.target.closest(".sort");
            if (sortBlocksMap.has(sortBlock)) {
                sort = sortBlocksMap.get(sortBlock);
            } else {
                sort = new Sort();
                sortBlocksMap.set(sortBlock, sort);
            }
            sort.setCurrentActive(sortButton);
            sort.toggleSortMode(sortButton);
        }
    }
})()

// video-player
;(function() {
    'use strict';
    window.addEventListener("DOMContentLoaded", pageLoadHandler);

    function pageLoadHandler() {
        document.querySelectorAll(".video-player__wrapper").forEach(processVideoPlayerWrapper)
    }

    function processVideoPlayerWrapper(videoPlayerWrapper) {
        let videoId = videoPlayerWrapper.querySelector(".video-player__player").id;
        let previewElement = videoPlayerWrapper.querySelector(".video-player__preview");
        setYoutubePreview(videoId, previewElement);

        videoPlayerWrapper.addEventListener("click", videoClickHandler);
    }

    function setYoutubePreview(videoId, previewElement) {
        previewElement.style.background = "url('https://img.youtube.com/vi/" + videoId + "/hqdefault.jpg') center center / cover no-repeat";
    }

    function videoClickHandler(e) {
        let wrapper = e.target.closest(".video-player__wrapper");
        let preview = wrapper.querySelector(".video-player__preview");
        let playButton = wrapper.querySelector(".video-player__button_play");
        let player = wrapper.querySelector(".video-player__player");
        player.src += "&autoplay=1";
        hideElement(playButton);
        hideElement(preview);
        wrapper.removeEventListener("click", videoClickHandler);
    }
})();

// article
;(function() {
    'use strict';

    initArticlesState();

    let articleClassToggler = new ElementClassToggler(
        "article_opened",
        "article_closed",
        "article__control",
        "article"
    );
    articleClassToggler.setEventListener();

    document.addEventListener("click", function(e) {
        let articleControl = e.target.closest(".article__control");
        if (articleControl === null) {
            return;
        }

        toggleArticleControlState(articleControl);
    });

    function toggleArticleControlState(articleControl) {
        let article = articleControl.closest(".article");
        if (article.classList.contains("article_closed")) {
            articleControl.innerHTML = "Весь текст &rarr;";
        } else {
            articleControl.innerHTML = "Скрыть &rarr;";
        }
    }

    function initArticlesState() {
        let articleClassToggler = new ElementClassToggler(
            "article_opened",
            "article_closed",
            "article__control",
            "article"
        );
        document.querySelectorAll(".article").forEach(function(articleItem) {
            let articleControl = articleItem.querySelector(".article__control");
            articleClassToggler.toggleClass(articleControl);
            toggleArticleControlState(articleControl);
        });
    }

})();

// genres
;(function() {
    'use strict';

    let genresListObj = new ElementClassToggler("genres-list__item_opened", 
        "genres-list__item_closed", 
        "genres-list__control", 
        "genres-list__item");

    genresListObj.setEventListener();
    initGenresListItemState(); 

    document.addEventListener("click", function(e) {
        let genresListControl = e.target.closest(".genres-list__control");

        if (genresListControl === null) {
            return;
        }

        toggleGenresListItemControlState(genresListControl);
    });

    function toggleGenresListItemControlState(genresListControl) {
        let genresListControlText = genresListControl.querySelector(".genres-list__control-text");
        let genresListItem = genresListControl.closest(".genres-list__item");

        if (genresListItem.classList.contains("genres-list__item_closed")) {
            genresListControlText.innerHTML = "Направления";
        } else {
            genresListControlText.innerHTML = "Свернуть";
        }
    }

    function initGenresListItemState() {
        let genresListItemClassToggler = new ElementClassToggler(
            "genres-list__item_opened", 
            "genres-list__item_closed", 
            "genres-list__control", 
            "genres-list__item"
        );

        document.querySelectorAll(".genres-list__item").forEach(function(genresListItem) {
            let genresListControl = genresListItem.querySelector(".genres-list__control");
            
            genresListItemClassToggler.toggleClass(genresListControl);
            toggleGenresListItemControlState(genresListControl);
        });
    }
})();

// player
;(function() {
    'use strict'

    class Player {

        constructor(playerId, trackListId) {
            this.eventTypes = this.setEventsTypeByDisplayType();

            this.playerState.isPlay = false;

            this.components.root = document.getElementById(playerId);
            this.components.audioElement = this.components.root.querySelector(".player__audio-element");
            this.components.audioSrc.srcMpeg = this.components.audioElement.querySelector(".player__src-mpeg");
            this.components.volumeControl.root = this.components.root.querySelector(".player__volume-control");
            this.components.volumeControl.volumeButton = this.components.volumeControl.root.querySelector(".player__control_volume");
            this.components.volumeControl.volumeLine = this.components.volumeControl.root.querySelector(".player__volume-line");
            this.components.volumeControl.volumeLineWidth = this.components.volumeControl.volumeLine.offsetWidth;
            this.components.volumeControl.volumeTrack = this.components.volumeControl.root.querySelector(".player__volume-track");
            this.components.volumeControl.volumeTrackLine = this.components.volumeControl.root.querySelector(".player__volume-track-line");
            this.components.volumeControl.volumePoint = this.components.volumeControl.root.querySelector(".player__volume-point");
            this.components.volumeControl.maxVolume = 1;

            this.components.playControl.root = this.components.root.querySelector(".player__play-controls");
            this.components.playControl.prevTrack = this.components.playControl.root.querySelector(".player__control_prev-track");
            this.components.playControl.nextTrack = this.components.playControl.root.querySelector(".player__control_next-track");
            this.components.playControl.playPause = this.components.playControl.root.querySelector(".player__control_play");
            this.components.playControl.tracklist = this.components.playControl.root.querySelector(".player__tracklist");
            this.components.playControl.timeline = this.components.root.querySelector(".player__timeline");
            this.components.playControl.progressLine = this.components.playControl.timeline.querySelector(".player__progress-line");
            this.components.playControl.progressTrack = this.components.playControl.timeline.querySelector(".player__progress-track");
            this.components.playControl.progressTrackLine = this.components.playControl.timeline.querySelector(".player__progress-track-line");
            this.components.playControl.timelineWidth = this.components.playControl.progressTrackLine.offsetWidth;
            this.components.playControl.progressPoint = this.components.playControl.timeline.querySelector(".player__progress-point");
            this.components.playControl.bufferTrack = this.components.root.querySelector(".player__buffer-track");

            this.components.playProgressInfo.trackTitle = this.components.root.querySelector(".player__track-title");
            this.components.playProgressInfo.trackSubtitle = this.components.root.querySelector(".player__track-subtitle");

            this.components.playProgressInfo.root = this.components.root.querySelector(".player__play-progress");
            this.components.playProgressInfo.currentProgress = this.components.playProgressInfo.root.querySelector(".player__current-progress");
            this.components.playProgressInfo.totalDuration = this.components.playProgressInfo.root.querySelector(".player__total-duration");

            this.components.playerTrackList.root = document.getElementById(trackListId);
            this.components.playerTrackList.list = this.components.playerTrackList.root.querySelector(".track-list__list");
                        
            this.downProgressPointHandler = this.downProgressPointHandler.bind(this);
            this.moveProgressPointHandler = this.moveProgressPointHandler.bind(this);
            this.upProgressPointHandler = this.upProgressPointHandler.bind(this);
            this.downVolumePointHandler = this.downVolumePointHandler.bind(this);
            this.moveVolumePointHandler = this.moveVolumePointHandler.bind(this);
            this.upVolumePointHandler = this.upVolumePointHandler.bind(this);
            this.showPlayerTrackList = this.showPlayerTrackList.bind(this);
            this.hidePlayerTrackList = this.hidePlayerTrackList.bind(this);
            this.toggleTracklist = this.toggleTracklist.bind(this);
            this.scrollToBottom = this.scrollToBottom.bind(this);
            this.playerLoadTracksFromTracklist = this.playerLoadTracksFromTracklist.bind(this);

            this.playerState.prevVolume = this.components.audioElement;
            this.components.audioElement.volume = getCookie("minty_current_volume_percent") || this.components.volumeControl.maxVolume;
        }

        components = {
            audioSrc: {},
            volumeControl: {},
            playControl: {},
            playInfo: {},
            playProgressInfo: {},
            playerTrackList: {},
        };

        playerState = {
            currentTrackRowInstances: {},
            trackQueue: [],
        };

        constants = {
            HIDE_CLASS: "visually-hidden",
            SEC_IN_MONTH: 2592000,
            TRACKLIST_ANIMATION_DURATION_MS: 250,
        };


        initPlayer() {
            this.components.playControl.playPause.addEventListener("click", () => {
                this.playPauseHandler();
            });

            this.components.audioElement.addEventListener("timeupdate", () => {
                this.playingHandler();
                this.changeBufferTrack();
            });

            this.components.audioElement.addEventListener("volumechange", () => {
                this.updateVolumeLine();
                this.updateVolumeIconByVolumeValue();
                setCookie("minty_current_volume_percent", this.components.audioElement.volume, {expires: this.constants.SEC_IN_MONTH});
            });

            this.components.audioElement.addEventListener("loadedmetadata", () => {
                this.components.playProgressInfo.currentProgress.innerHTML = this.timeInSecToFormat(this.components.audioElement.currentTime);
                this.components.playProgressInfo.totalDuration.innerHTML = this.timeInSecToFormat(this.components.audioElement.duration);
            });

            this.components.playControl.progressTrackLine.addEventListener("click", (event) => {
               this.setCurrentTimeValue(event);
            });

            this.components.volumeControl.volumeLine.addEventListener("click", (event) => {
                this.setCurrentVolumeValue(event);
             });

            this.components.playControl.progressPoint.addEventListener(this.eventTypes.down, this.downProgressPointHandler);

            this.components.volumeControl.volumePoint.addEventListener(this.eventTypes.down, this.downVolumePointHandler);

            this.components.volumeControl.volumeButton.addEventListener("click", (event) => {
                if (this.components.audioElement.volume > 0) {
                    this.playerState.prevVolume = this.components.audioElement.volume;
                    this.components.audioElement.volume = 0;
                } else {
                    this.components.audioElement.volume = this.playerState.prevVolume;
                }
            })

            this.components.playControl.tracklist.addEventListener("click", this.toggleTracklist);

            document.addEventListener("click", (event) => {
                let currentTrackRowControl = event.target.closest(".track-row__control");
                let currentTrackElement = event.target.closest(".track-row");
                let closestTracklist = event.target.closest(".track-list");
                
                if (currentTrackRowControl === null) {
                    return;
                }

                if (this.playerState.currentTrackRowInstances.externalTrackRow !== undefined && 
                (this.playerState.currentTrackRowInstances.externalTrackRow === currentTrackElement || 
                this.playerState.currentTrackRowInstances.internalTrackRow === currentTrackElement)) {
                    this.playPauseHandler();
                } else {
                    this.toggleActiveStateCurrentTrackElement();
                    this.updateTrackData(currentTrackElement);
    
                    if (currentTrackRowControl.closest(".track-list_in-player") === null) {
                        if (this.playerState.currentTrackRowInstances.externalTrackRow !== undefined &&
                            this.playerState.currentTrackRowInstances.internalTrackRow !== undefined) {
                            this.forcePauseStateMusicButtonControl();
                        }
                        this.playerState.currentTrackRowInstances.externalTrackRow = currentTrackElement;

                        if (closestTracklist !== this.playerState.currentPlayerTracklist) {
                            this.playerLoadTracksFromTracklist(closestTracklist);
                        }

                        this.playerState.currentTrackRowInstances.internalTrackRow = this.playerState.currentTrackRowInstances.externalTrackRow.clone;
                        this.loadTrackInPlayer();
                        this.components.audioElement.pause();
                        this.playPauseHandler();
                    }
                    
                    if (currentTrackRowControl.closest(".track-list_in-player") !== null) {
                        this.forcePauseStateMusicButtonControl();
                        this.playerState.currentTrackRowInstances.externalTrackRow = currentTrackElement.original;
                        this.playerState.currentTrackRowInstances.internalTrackRow = currentTrackElement;
                        this.loadTrackInPlayer();
                        this.playPauseHandler();
                    }
                    this.toggleActiveStateCurrentTrackElement();
                }
            });

            this.components.audioElement.addEventListener("play", (event) => {
                this.musicButtonControlToggler(
                    this.playerState.currentTrackRowInstances.externalTrackRow.querySelector(".playControl"),
                    this.playerState.currentTrackRowInstances.internalTrackRow.querySelector(".playControl"),
                    this.components.playControl.playPause);
            });

            this.components.audioElement.addEventListener("pause", (event) => {
                this.musicButtonControlToggler(
                    this.playerState.currentTrackRowInstances.externalTrackRow.querySelector(".playControl"),
                    this.playerState.currentTrackRowInstances.internalTrackRow.querySelector(".playControl"),
                    this.components.playControl.playPause);
            });

            this.components.audioElement.addEventListener("ended", (event) => {
                this.nextTrack();
            });

            this.components.playControl.prevTrack.addEventListener("click", (event) => {
                if (this.components.playControl.prevTrack.classList.contains("player__control_inactive")) {
                    return;
                }

                this.prevTrack();
            });

            this.components.playControl.nextTrack.addEventListener("click", (event) => {
                if (this.components.playControl.nextTrack.classList.contains("player__control_inactive")) {
                    return;
                }

                this.nextTrack();
            });

            this.updateProgressSliderStartState();
            this.updateVolumeSliderStartState();

            this.playerState.isGrabbed = false;

            // initialize start parameters
            this.components.playControl.progressPoint.style.left = this.playerState.currentProgressPointPositionX + "px";
            this.components.playControl.progressTrack.style.width = this.playerState.currentProgressTrackWidth + "px";
            this.updateVolumeLine();
            this.updateVolumeIconByVolumeValue();
            this.hidePlayerTrackList();

            this.playerTrackListUpdateState();
            this.playerControlsUpdateState();
        }

        playPauseHandler() {
            if (this.components.playerTrackList.list.children.length === 0) {
                return;
            }

            if (this.components.audioElement.paused === true) {
                this.components.audioElement.play();
            } else if (this.components.audioElement.paused === false) {
                this.components.audioElement.pause();
            }
        }

        musicButtonControlToggler(...musicButton) {
            for (let element of musicButton) {
                if (this.components.audioElement.paused === true) {
                    element.classList.add("playControl__pause");
                    element.classList.remove("playControl__play");
                } else if (this.components.audioElement.paused === false) {
                    element.classList.add("playControl__play");
                    element.classList.remove("playControl__pause");
                }
            } 
        }

        forcePauseStateMusicButtonControl() {
            this.components.audioElement.pause();
            this.musicButtonControlToggler(      
                this.playerState.currentTrackRowInstances.externalTrackRow.querySelector(".playControl"),
                this.playerState.currentTrackRowInstances.internalTrackRow.querySelector(".playControl"),
                this.components.playControl.playPause);
        }

        setCurrentTimeValue(event) {
            this.components.audioElement.currentTime = event.offsetX * this.components.audioElement.duration / this.components.playControl.timelineWidth;
        }

        downProgressPointHandler(event) {
            this.playerState.moveStartX = (typeof event.touches !== "undefined") ? event.touches[0].pageX : event.pageX;
            this.playerState.moveStartValue = this.components.audioElement.currentTime;
            this.playerState.isGrabbed = true;

            document.addEventListener(this.eventTypes.move, this.moveProgressPointHandler);
            document.addEventListener(this.eventTypes.up, this.upProgressPointHandler);
        }

        moveProgressPointHandler(event) {
            this.playerState.moveCurrentX = (typeof event.touches !== "undefined") ? event.touches[0].pageX : event.pageX;
            let currentOffsetX = this.getCoordinateOffset(this.playerState.moveStartX, this.playerState.moveCurrentX);
            let actualOffset = this.playerState.currentProgressPointPositionX + currentOffsetX;

            if (actualOffset < 0) {
                this.components.playControl.progressPoint.style.left = 0 + "px";
            } else if (actualOffset > this.components.playControl.timelineWidth) {
                this.components.playControl.progressPoint.style.left = this.components.playControl.timelineWidth + "px";
            } else {
                this.components.playControl.progressPoint.style.left = actualOffset + "px";
            }
        }
        
        upProgressPointHandler(event) {
            this.playerState.isGrabbed = false;
            this.components.audioElement.currentTime = this.playerState.moveStartValue + 
                this.getCoordinateOffset(this.playerState.moveStartX, this.playerState.moveCurrentX) * 
                this.components.audioElement.duration / 
                this.components.playControl.timelineWidth; 
                
            this.updateProgressSliderStartState();

            delete this.playerState.moveStartX;
            delete this.playerState.moveStartValue;
            delete this.playerState.moveCurrentX;
            
            document.removeEventListener(this.eventTypes.move, this.moveProgressPointHandler);
            document.removeEventListener(this.eventTypes.up, this.upProgressPointHandler);
        }

        playingHandler() {
            this.updateProgressLine();
            this.updateProgressInfo();
        
        }

        updateProgressSliderStartState() {
            this.playerState.currentProgressPointPositionX = 0;
            this.playerState.currentProgressTrackWidth = 0;
        }

        updateProgressLine() {
            let progressValue = Math.floor(this.components.audioElement.currentTime * this.components.playControl.timelineWidth / this.components.audioElement.duration);
            this.playerState.currentProgressTrackWidth = progressValue;
            this.components.playControl.progressTrack.style.width = progressValue + "px";
            if (this.playerState.isGrabbed === false) {
                this.playerState.currentProgressPointPositionX = progressValue;
                this.components.playControl.progressPoint.style.left = progressValue + "px";
            } 
        }

        updateProgressInfo() {
            this.components.playProgressInfo.currentProgress.innerHTML = this.timeInSecToFormat(this.components.audioElement.currentTime);
        }

        changeBufferTrack() {
            this.components.playControl.bufferTrack.style.width = Math.floor(this.components.audioElement.buffered.end(this.components.audioElement.buffered.length - 1) * this.components.playControl.timelineWidth / this.components.audioElement.duration) + "px";
        }

        setCurrentVolumeValue(event) {
            if (event.srcElement !== this.components.volumeControl.volumePoint) {
                this.components.audioElement.volume = (event.offsetX * this.components.volumeControl.maxVolume / this.components.volumeControl.volumeLineWidth).toFixed(4);
            }
            this.updateVolumeSliderStartState();
        }

        downVolumePointHandler(event) {
            this.playerState.moveStartX = (typeof event.touches !== "undefined") ? event.touches[0].pageX : event.pageX;
            this.playerState.moveStartValue = this.components.audioElement.volume;
            this.playerState.isGrabbed = true;

            document.addEventListener(this.eventTypes.move, this.moveVolumePointHandler);
            document.addEventListener(this.eventTypes.up, this.upVolumePointHandler);
        }

        moveVolumePointHandler(event) {
            this.playerState.moveCurrentX = (typeof event.touches !== "undefined") ? event.touches[0].pageX : event.pageX;
            let currentOffsetX = this.getCoordinateOffset(this.playerState.moveStartX, this.playerState.moveCurrentX);
            let actualOffset = this.playerState.currentVolumePointPositionX + currentOffsetX;

            this.updateVolumeLine("currentOffsetX", currentOffsetX);

            if (actualOffset < 0) {
                this.components.volumeControl.volumePoint.style.left = 0 + "px";
                this.components.audioElement.volume = 0;
            } else if (actualOffset > this.components.volumeControl.volumeLineWidth) {
                this.components.volumeControl.volumePoint.style.left = this.components.volumeControl.volumeLineWidth + "px";
                this.components.audioElement.volume = this.components.volumeControl.maxVolume;
            } else {
                this.components.volumeControl.volumePoint.style.left = actualOffset + "px";
                this.components.audioElement.volume = (actualOffset * this.components.volumeControl.maxVolume) / this.components.volumeControl.volumeLineWidth;
            }
        } 

        upVolumePointHandler(event) {
            this.playerState.isGrabbed = false;
            let actualVolume = this.playerState.moveStartValue + 
                this.getCoordinateOffset(this.playerState.moveStartX, this.playerState.moveCurrentX) * 
                this.components.volumeControl.maxVolume / this.components.volumeControl.volumeLineWidth;

            if (actualVolume < 0) {
                this.components.audioElement.volume = 0;
            } else if (actualVolume > this.components.volumeControl.maxVolume) {
                this.components.audioElement.volume = this.components.volumeControl.maxVolume.toFixed(4);
            } else {
                this.components.audioElement.volume = actualVolume.toFixed(4); 
            }

            this.updateVolumeSliderStartState();

            delete this.playerState.moveStartX;
            delete this.playerState.moveStartValue;
            delete this.playerState.moveCurrentX;

            document.removeEventListener(this.eventTypes.move, this.moveVolumePointHandler);
            document.removeEventListener(this.eventTypes.up, this.upVolumePointHandler);
        }

        updateVolumeSliderStartState() {
            this.playerState.currentVolumePointPositionX = Math.floor(this.components.audioElement.volume * this.components.volumeControl.volumeLineWidth / this.components.volumeControl.maxVolume);
            this.playerState.currentVolumeTrackWidth = Math.floor(this.components.audioElement.volume * this.components.volumeControl.volumeLineWidth / this.components.volumeControl.maxVolume);
        }

        updateVolumeLine() {
            let volumeLineValue = Math.floor(this.components.audioElement.volume * this.components.volumeControl.volumeLineWidth / this.components.volumeControl.maxVolume);
            this.playerState.currentVolumeTrackWidth = volumeLineValue;
            this.components.volumeControl.volumeTrack.style.width = volumeLineValue + "px";

            if (this.playerState.isGrabbed === false) {
                this.components.volumeControl.volumePoint.style.left = volumeLineValue + "px";
            }

            this.playerState.currentVolumeTrackWidth = volumeLineValue; //
        }

        updateVolumeIconByVolumeValue() {
            if (this.components.audioElement.volume === 0 && !this.components.volumeControl.volumeButton.classList.contains("player__control_muted")) {                 
                this.components.volumeControl.volumeButton.classList.add("player__control_muted");
                this.components.volumeControl.volumeButton.classList.remove("player__control_low-volume");
                this.components.volumeControl.volumeButton.classList.remove("player__control_high-volume");
            } else if (this.components.audioElement.volume < 0.5 && !this.components.volumeControl.volumeButton.classList.contains("player__control_low-volume")) {
                this.components.volumeControl.volumeButton.classList.add("player__control_low-volume");
                this.components.volumeControl.volumeButton.classList.remove("player__control_high-volume");
                this.components.volumeControl.volumeButton.classList.remove("player__control_muted");
            } else if (this.components.audioElement.volume >= 0.5 && !this.components.volumeControl.volumeButton.classList.contains("player__control_high-volume")) {
                this.components.volumeControl.volumeButton.classList.add("player__control_high-volume");
                this.components.volumeControl.volumeButton.classList.remove("player__control_low-volume");
                this.components.volumeControl.volumeButton.classList.remove("player__control_muted");
            }
        }

        timeInSecToFormat(timeInSec) {
            let resultTimeString = "";
            let timeInSecFloored = Math.floor(timeInSec);
            let hours = Math.floor(timeInSecFloored / 3600);
            hours = (hours > 0) ? hours : 0;
            timeInSecFloored -= hours * 3600;
            let minutes = Math.floor(timeInSecFloored / 60);
            minutes = (minutes > 0) ? minutes : 0;
            timeInSecFloored -= minutes * 60;
            let seconds = timeInSecFloored;

            if (hours !== 0) {
                resultTimeString += ((hours.toString().length < 2) ? "0" + hours : hours) + ":";
            }
            resultTimeString += ((minutes.toString().length < 2) ? "0" + minutes : minutes) + ":" + ((seconds.toString().length < 2) ? "0" + seconds : seconds);

            return resultTimeString;
        }

        showElement(element) {
            element.classList.remove(this.constants.HIDE_CLASS);
        }

        hideElement(element) {
            element.classList.add(this.constants.HIDE_CLASS);
        }

        setEventsTypeByDisplayType() {
            let events = {};
    
            if ('ontouchstart' in window || navigator.msMaxTouchPoints) {
                events.down = "touchstart";
                events.up = "touchend";
                events.move = "touchmove";
            } else {
                events.down = "mousedown";
                events.up = "mouseup";
                events.move = "mousemove";
            }
    
            return events;
        }

        getCoordinateOffset(from, to) {
            if (to === undefined) {
                to = from;
            }
            return (to !== undefined) ? to - from : 0;
        }

        toggleTracklist() {
            if (this.components.playControl.tracklist.classList.contains("player__control_inactive")) {
                return;
            }

            if (this.components.playerTrackList.root.classList.contains("trackListHidden")) {
                this.showPlayerTrackList();
                setTimeout(this.scrollToBottom, this.constants.TRACKLIST_ANIMATION_DURATION_MS);
            } else {
                this.hidePlayerTrackList();
            }
        }

        showPlayerTrackList() {
            this.components.playerTrackList.root.style.transition = "margin-bottom " + this.constants.TRACKLIST_ANIMATION_DURATION_MS + "ms ease-in-out";
            this.components.playerTrackList.root.style.paddingBottom = "24px";
            this.components.playerTrackList.root.style.marginBottom = (this.components.root.offsetHeight - 16) + "px";
            this.components.playerTrackList.root.classList.remove("trackListHidden");
        }

        scrollToBottom() {
            window.scrollBy({
                top: document.scrollingElement.offsetHeight,
                behavior: 'smooth',
            });
        }

        hidePlayerTrackList() {
            this.components.playerTrackList.root.style.marginBottom = -(this.components.playerTrackList.root.offsetHeight - this.components.root.offsetHeight) + "px";
            this.components.playerTrackList.root.classList.add("trackListHidden");
        }

        playerTrackListUpdateState() { 
            if (this.components.playerTrackList.list.children.length === 0) {
                this.lockControl(this.components.playControl.tracklist);
                this.hidePlayerTrackList();
            } else {
                this.unlockControl(this.components.playControl.tracklist);
                this.unlockControl(this.components.playControl.playPause);
            }
        }
        playerControlsUpdateState() {
            if (this.components.playerTrackList.list.children.length === 0) { 
                this.lockControl(this.components.playControl.prevTrack);
                this.lockControl(this.components.playControl.nextTrack);
                this.lockControl(this.components.playControl.playPause);
                return;
            } 

            if (this.playerState.currentTrackRowInstances.internalTrackRow.nextSibling === null) {
                this.lockControl(this.components.playControl.nextTrack);
            } else {
                this.unlockControl(this.components.playControl.nextTrack);
            }

            if (this.playerState.currentTrackRowInstances.internalTrackRow.previousSibling === null) {
                this.lockControl(this.components.playControl.prevTrack);
            } else {
                this.unlockControl(this.components.playControl.prevTrack);
            }

        }

        lockControl(control) {
            control.classList.add("player__control_inactive")
        }

        unlockControl(control) {
            control.classList.remove("player__control_inactive")
        }

        playerLoadTracksFromTracklist(tracklist) {
            this.components.playerTrackList.list.innerHTML = "";
            let list = tracklist.querySelector(".track-list__list");
            this.playerState.currentPlayerTracklist = tracklist;
            this.playerState.trackQueue = [];

            for (let track of list.children) {
                let trackClone = track.cloneNode(true);
                track.clone = trackClone;
                trackClone.original = track;
                this.components.playerTrackList.list.append(trackClone);
            }
            
            this.playerTrackListUpdateState();
            this.hidePlayerTrackList();
        }

        loadTrackInPlayer() {
                this.components.audioSrc.srcMpeg.setAttribute("src", this.playerState.currentTrack.dataUrl);
                this.components.audioElement.load();

                this.components.playProgressInfo.trackTitle.setAttribute("title", this.playerState.currentTrack.dataName);
                this.components.playProgressInfo.trackTitle.innerHTML = this.playerState.currentTrack.dataName;
                
                this.components.playProgressInfo.trackSubtitle.setAttribute("title", this.playerState.currentTrack.dataArtist);
                this.components.playProgressInfo.trackSubtitle.innerHTML = this.playerState.currentTrack.dataArtist;

                this.playerControlsUpdateState();
        }

        updateTrackData(nextTrack) {
            this.playerState.currentTrack = {
                dataUrl: nextTrack.getAttribute("data-url"),
                dataAlbomId: nextTrack.getAttribute("data-albom-id"),
                dataId: nextTrack.getAttribute("data-id"),
                dataName: nextTrack.getAttribute("data-name"),
                dataArtist: nextTrack.getAttribute("data-artist"),
                dataAlbom: nextTrack.getAttribute("data-albom"),
            }
        }

        nextTrack() {
            let nextTrack = this.getNextTrack();
            if (nextTrack !== null) {
                this.forcePauseStateMusicButtonControl();
                this.playerState.currentTrackRowInstances.internalTrackRow = nextTrack;
                this.playerState.currentTrackRowInstances.externalTrackRow = nextTrack.original;
                this.updateTrackData(nextTrack);
                this.loadTrackInPlayer();
                this.playPauseHandler();
            } else {
                this.components.audioElement.currentTime = 0;
            }
        }

        getNextTrack() {
            return this.playerState.currentTrackRowInstances.internalTrackRow.nextSibling;
        }

        prevTrack() {
            let prevTrack = this.getPrevTrack();
            if (prevTrack !== null) {
                this.forcePauseStateMusicButtonControl();
                this.playerState.currentTrackRowInstances.internalTrackRow = prevTrack;
                this.playerState.currentTrackRowInstances.externalTrackRow = prevTrack.original;
                this.updateTrackData(prevTrack);
                this.loadTrackInPlayer();
                this.playPauseHandler();
            }
        }

        getPrevTrack() {
            return this.playerState.currentTrackRowInstances.internalTrackRow.previousSibling;
        }

        toggleActiveStateCurrentTrackElement() {
            if (!this.playerState.currentTrackRowInstances.internalTrackRow ||  
                !this.playerState.currentTrackRowInstances.externalTrackRow) {
                    return;
            }
            
            if (this.playerState.currentTrackRowInstances.internalTrackRow.classList.contains("track-row_active") &&
            this.playerState.currentTrackRowInstances.externalTrackRow.classList.contains("track-row_active")) {
                this.playerState.currentTrackRowInstances.internalTrackRow.classList.remove("track-row_active");
                this.playerState.currentTrackRowInstances.externalTrackRow.classList.remove("track-row_active");
            } else {
                this.playerState.currentTrackRowInstances.internalTrackRow.classList.add("track-row_active");
                this.playerState.currentTrackRowInstances.externalTrackRow.classList.add("track-row_active");
            }

        }
    }

    let player = new Player("mainPlayer", "mainPlayerTrackList");
    player.initPlayer();

})();