'use strict'

const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const debug = require('gulp-debug');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const babel = require('gulp-babel');
const flatten = require('gulp-flatten');

function buildStyles() {
    return gulp.src('./src/style.scss')
        .pipe(sourcemaps.init())
            .pipe(debug({title: 'src'}))
            .pipe(sass().on('error', sass.logError))
            .pipe(debug({title: 'scss'}))
            .pipe(autoprefixer({
                cascade: false
            }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build'));
}

function images() {
    return gulp.src('./src/**/*.{svg,png}')
        .pipe(flatten({ includeParents: 1}))
        .pipe(gulp.dest(function(file) {
            return "./build/img/";
        }));
}

function script() {
    return gulp.src('./src/script.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest('./build/'));
}

function html() {
    return gulp.src('./src/index.html')
        .pipe(gulp.dest('./build/'));
}

function fonts() {
    return gulp.src('./src/fonts/*')
        .pipe(gulp.dest('./build/fonts/'));
}

function serve() {
    browserSync.init({
        server: 'build',
    });

    browserSync.watch('build/**/*.*').on('change', browserSync.reload);
}

function watch() {
    gulp.watch('./src/**/*.{scss,js,html}', gulp.series('buildStyles', 'script', 'html', 'images'));
}

exports.build = gulp.series(buildStyles, script, html, images, fonts);
exports.buildStyles = buildStyles;
exports.script = script;
exports.html = html;
exports.serve = serve;
exports.watch = watch;
exports.images = images;
exports.fonts = fonts;